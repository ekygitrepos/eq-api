class HomesController < ApplicationController
  before_action :set_home, only: [:show, :update, :destroy]
  before_action :authenticate_jwt

  # GET /homes
  def index
    @homes = Home.all

    render json: @homes
  end

  def auth
    app = HomeServices.auth(params)

    if app.present?
      # ===========================
      # Check if app status ok?
      # ===========================
      if app.status
        data = {id: app.id, keyword: app.keyword,encrypt: app.encrypt}

        exp = Time.now.to_i + 1 * 3600
        exp_payload = { data: data, exp: exp }

        hmac_secret = "#{ENV['hmac_secret']}"

        token = JWT.encode exp_payload, hmac_secret, 'HS256'

        payload = {
          token: token,
          exp: exp
        }

        ret = {
          status:true,
          message:"Well Hello, You're Success to Generate Token",
          payload:payload
        }

      else
        ret = {
          status: false,
          message: "Eky says : Sorry, App doesn't exist or blocked! (Error : EQ-100)",
          payload: nil
        }
      end

    else
      ret = {
        status: false,
        message: "Eky says : Sorry, Something wrong on this API !! (Error : EQ-200)",
        payload: nil
      }
    end

    render json: ret
    return
  rescue Exception => ex
    render json: {
      status: false,
      message: ex,
      payload: nil
    }
    return
  end
  # GET /homes/1
  def show
    render json: @home
  end

  # POST /homes
  def create
    @home = Home.new(home_params)

    if @home.save
      render json: @home, status: :created, location: @home
    else
      render json: @home.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /homes/1
  def update
    if @home.update(home_params)
      render json: @home
    else
      render json: @home.errors, status: :unprocessable_entity
    end
  end

  # DELETE /homes/1
  def destroy
    @home.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_home
      @home = Home.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def home_params
      params.require(:home).permit(:keyword, :encrypt)
    end
end
