class CreateHomes < ActiveRecord::Migration[6.1]
  def change
    create_table :homes do |t|
      t.string :keyword
      t.string :encrypt

      t.timestamps
    end
  end
end
