class AddStatusToHomes < ActiveRecord::Migration[6.1]
  def change
    add_column :homes, :status, :string
  end
end
