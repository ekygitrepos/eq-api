Rails.application.routes.draw do
  resources :homes
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root to: 'application#index'
  resources :users, param: :_username
  post '/auth', to: 'homes#auth'
  get '/*a', to: 'application#not_found'
end
